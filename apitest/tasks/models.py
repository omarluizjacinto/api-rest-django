from django.db import models
import uuid

# Create your models here.

class Tasks(models.Model):
    IdTask       = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True) 
    Title        = models.CharField(max_length=50) 
    Notes        = models.CharField(max_length=50)
    Priority     = models.IntegerField()
    RemindMeOn   = models.DateTimeField() 
    ActivityType = models.CharField(max_length=50)
    Status       = models.CharField(max_length=50)
    TaskList     = models.ForeignKey('taskList.TaskList', on_delete=models.CASCADE, verbose_name='TaskList')
    Tag          = models.ForeignKey('tags.Tags', on_delete=models.CASCADE, verbose_name='Tag')

    def __str__(self):
        return (self.Title)
    


from rest_framework.serializers import ModelSerializer
from tasks.models import Tasks


class TasksSerializer(ModelSerializer):
    class Meta:
        model = Tasks
        fields = ('IdTask', 'Title', 'Notes', 'Priority', 'RemindMeOn', 'ActivityType', 'Status', 'TaskList', 'Tag')


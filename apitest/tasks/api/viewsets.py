from rest_framework.viewsets import ModelViewSet
from tasks.models import Tasks

from .serializers import TasksSerializer

class TasksViewSet(ModelViewSet):
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer

from django.db import models
import uuid


# Create your models here.

class Tags(models.Model):
    IdTag = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    Name = models.CharField(max_length=150)
    Count = models.IntegerField()

    def __str__(self):
        return self.Name


    


from rest_framework.serializers import ModelSerializer
from tags.models import Tags


class TagsSerializer(ModelSerializer):
    class Meta:
        model = Tags
        fields = ('IdTag', 'Name', 'Count')


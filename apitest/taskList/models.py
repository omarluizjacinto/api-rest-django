from django.db import models
import uuid

# Create your models here.

class TaskList(models.Model):
    IdTaskList = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    Name = models.CharField(max_length=50)

    def __str__(self):
        return (self.Name)

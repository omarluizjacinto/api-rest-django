
from rest_framework.serializers import ModelSerializer
from taskList.models import TaskList


class TaskListSerializer(ModelSerializer):
    class Meta:
        model = TaskList
        fields = ('IdTaskList', 'Name',)


from rest_framework.viewsets import ModelViewSet
from taskList.models import TaskList

from .serializers import TaskListSerializer

class TaskListViewSet(ModelViewSet):
    queryset = TaskList.objects.all()
    serializer_class = TaskListSerializer
